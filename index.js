const express = require("express");
const app = express();
const cors = require("cors");
const session = require("express-session");
const http = require("http");
require('dotenv').config();

// database.js
const { postCallback } = require("./database");

//http통신부분 - 로그인
const corsOptions = {
  origin: true,
  credentials: true
}

app.use(
  session({
    resave: false,
    saveUninitialized: true,
    secret: "hamletshu",
    cookie: {
      httpOnly: true,
      secure: false
    }
  })
)
app.use(cors(corsOptions));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));


const server = http.createServer(app);

//소켓통신부분 - 채팅
const socket = require("socket.io");
const io = socket(server);

io.on("connection", (socket) => {
  console.log("소켓 연결 완료");
  let userName;

  //사용자 채팅방 입장 이벤트
  socket.on("join", (name) => {
    userName = name;
    console.log(`${userName}유저가 들어왔습니다`);

    // 이전 채팅기록을 전송
    postCallback(`${process.env.DATABASE_IP}room/findAll`, {}, (json) => {
      const parseJson = json.map((m) => {
        return {
            sender: m.sender,
            text: m.chatMessage,
        };
      });

      socket.emit("initMessage", parseJson);
      
      //채팅방 모두에게 채팅방 입장메세지 전송
      socket.broadcast.emit("message", {
        sender: "admin",
        text: `${name}, has joined!`
      });
    });

    

  });

  //사용자 메세지 발송 이벤트
  socket.on("sendMessage", (receiveInfo) => {
    postCallback(`${process.env.DATABASE_IP}room/sendMessage`, { sender: receiveInfo.sender, chatMessage: receiveInfo.text }, (json) => {
      if (json.message === "false") {
        console.log(receiveInfo, "채팅메세지 디비저장에 실패하였습니다.")
      }
      io.emit("message", {
        sender: receiveInfo.sender, text: receiveInfo.text
        }
      );
      console.log({ sender: receiveInfo.sender, text: receiveInfo.text });
    });
  });


  //사용자 종료 이벤트
  socket.on("disconnect", () => {
    console.log(`${userName}유저가 떠났습니다`);
    io.emit("message", {
      sender: "admin",
      text: `${userName} has left.`,
    });
  });

});



app.use(require("./router"));
const PORT = process.env.PORT;
server.listen(PORT, () => console.log(`server has started on port ${PORT}`));
