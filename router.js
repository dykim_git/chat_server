const express = require("express");
const router = express.Router();
const { json } = require('express');

// database.js
const { postCallback } = require("./database");

//로그인시 접속 주소
router.post("/", async (req, res) => {
  try {
    //로그인 데이터베이스통신
    postCallback(`${process.env.DATABASE_IP}user/login`, { name: req.body.name, password: req.body.password }, (json) => {
      if (!json._id) {
        res.json({ name: req.body.name, message: 'fail' });
        return
      } else {
        res.json({ name: req.body.name, message: 'success' });
      }
    });
  } catch (err) {
    console.log(err);
    res.json({ name: "error", message: "로그인중 서버에러" });
  }
});

//채팅시 접속주소
router.get("/", (req, res) => {
  res.send("server is runnning");
});

module.exports = router;
